<?php

namespace App\Http\Controllers;

use App\Models\jenis_batik;
use Illuminate\Http\Request;

class JenisBatikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request->all();
        $Data = jenis_batik::created($data);

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\jenis_batik  $jenis_batik
     * @return \Illuminate\Http\Response
     */
    public function show(jenis_batik $jenis_batik)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\jenis_batik  $jenis_batik
     * @return \Illuminate\Http\Response
     */
    public function edit(jenis_batik $jenis_batik)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\jenis_batik  $jenis_batik
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, jenis_batik $jenis_batik)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\jenis_batik  $jenis_batik
     * @return \Illuminate\Http\Response
     */
    public function destroy(jenis_batik $jenis_batik)
    {
        //
    }
}
