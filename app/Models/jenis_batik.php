<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class jenis_batik extends Model
{
    protected $table = "jenis_batiks";
    
    protected $fillable = [
        'nama', 'alat', 'motif', 'asal'
    ];
}
